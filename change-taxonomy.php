<?php
/**
 * Plugin Name: Change Taxonomy
 * Author: Kamrul Hasan
 */
register_activation_hook( __FILE__, function () {
	add_action( 'activated_plugin', function () {
		wp_redirect( add_query_arg( array(
			'action' => 'change-taxonomy'
		), get_site_url() ) );
		exit();
	} );
} );
function change_taxonomy() {
	if ( ! empty( $_GET['action'] ) && $_GET['action'] == 'change-taxonomy' ) {
	//define( 'DOING_AJAX', true );
		global $wpdb;

        $table_name = 'wp_term_taxonomy';
        $new_taxonomy = 'event_category';
        $old_taxonomy = 'dfw_event_type';

//        $query = "UPDATE  $table_name SET  `taxonomy` =  '$new_taxonomy' WHERE  `taxonomy` = '$old_taxonomy'";
//        $reminders = $wpdb->query( $wpdb->prepare( $query, 0 ) );
        $reminders = $wpdb->update($table_name, array('taxonomy' => $new_taxonomy), array('taxonomy' => $old_taxonomy));

		wp_die(var_dump($reminders), true);
	}
}

add_action( 'wp', 'change_taxonomy' );
